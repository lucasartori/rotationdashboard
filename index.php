<!DOCTYPE html>
<html>
<head>
  <title>Rotation</title>
       <style type="text/css">
            body, html
            {
                margin: 0; padding: 0; height: 100%; overflow: hidden;
            }

            #rotate
            {
                position:absolute; left: 0; right: 0; bottom: 0; top: 0px; 
            }
        </style>  
</head>
<body >   
  <div id="content">
      <iframe id="rotate" width="100%" height="100%" frameborder="0"></iframe>
  </div>
  
<script language="javascript">
var urls = ['reviewDashboard/','bambooDashboard/', 
            'newRelicDashboard/'];
var pos = 0;
//test
  
function next()
{
  if(pos == urls.length) pos = 0; // reset the counter
  document.getElementById('rotate').src = urls[pos];
  pos++;
}
  
window.onload = function(e){ 

next();

setInterval(next, 15000); // every 15 seconds


}

</script>  
</body>
</html>
