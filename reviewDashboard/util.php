<?php
require_once 'story.php';


function getCURLDataFromPresetFilter(){
  $url = "https://leisure.atlassian.net/rest/api/2/search";

	$headers = array(
    'Accept: application/json',
    'Content-Type: application/json',
    'Authorization: Basic YmVsdmlsbGEtYWxsOmhlM2c0UlZreWthSw=='
  );

	$url2 = "https://leisure.atlassian.net/rest/api/2/search?expand=changelog&jql=project%20=%20WES%20AND%20status%20=%20%22Ready%20for%20review%22%20AND%20NOT%20updated%20%3C%20-30d%20ORDER%20BY%20updated%20DESC,%20Rank%20ASC";
	



  $ch = curl_init();
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_VERBOSE, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);  
//  curl_setopt($ch,CURLOPT_POSTFIELDS, '{"jql":"project = WES AND issuetype in (Bug, Maintenance, Operations) AND NOT (status = Closed AND updated < -30d) ORDER BY Rank ASC"}');
//   curl_setopt($ch,CURLOPT_POSTFIELDS, '{"expand":"names","jql":"project = WES AND status = \"Ready for review\" AND NOT updated < -30d ORDER BY updated DESC, Rank ASC"}');
//  	curl_setopt($ch,CURLOPT_POST, 2);
//   curl_setopt($ch,CURLOPT_POSTFIELDS, $field_string);
  curl_setopt($ch, CURLOPT_URL, $url2);

  /* execute the request */
  $result = curl_exec($ch);
  curl_close($ch);
  return $result;
}




function convertStories($jsonIssues){
  $issues =[];
  foreach ($jsonIssues as $story) {
    array_push ($issues, new Story($story));
  }
  return $issues;
}
