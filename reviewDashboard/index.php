<?php
require_once 'util.php';
require_once 'story.php';

$result = getCURLDataFromPresetFilter();
$strDatas = json_decode($result);
// echo "<pre>";
// print_r($result);
// echo "</pre>";
$issues = convertStories($strDatas->issues);

?>	
<!DOCTYPE html>
<html>
<head>
	<title>Waiting for Review</title>

  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

  <link href='https://fonts.googleapis.com/css?family=Source+Code+Pro' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Nunito' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  
  </head>
<body>

<nav class="navbar navbar-default navbar-static-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <p class="navbar-text">Waiting for Review</p>
    </div>
  </div>
</nav>	
	
  <div class="container-fluid">
    <div class="row">	
	<?php
// print "<pre>";
// print_r($strDatas->issues);
// print "</pre>";

			
			
  foreach ($issues as $story) {
    $story->printStoryCard();
  }			
?>
			
			
      </div>
    </div>
</body>
</html>


