<?php
class Story {

  public $key = '';
  public $summary = '';
  public $type = '';
  public $parentStory = '';
  public $epic = '';
  public $points = '';
  public $status = '';
  public $timeEstimate = '';
  public $developers = '';
  public $createdDate = '';
  public $updatedDate = '';
  public $statusChangeDate = "";
  
  public function __construct ($jsonStory){
    $this->key = $jsonStory->key;
    $this->summary = $jsonStory->fields->summary;
    $this->type = $jsonStory->fields->issuetype->name;

    if (property_exists ($jsonStory->fields, 'parent')){
     $this->parentStory =  $jsonStory->fields->parent->key;
    }
    $this->timeEstimate = round($jsonStory->fields->timeestimate/28800,1);
    $this->epic = $jsonStory->fields->customfield_10008;
    $this->points = $jsonStory->fields->customfield_10004;
    $this->status = $jsonStory->fields->status->name;
    
    $this->createdDate = $jsonStory->fields->created;
    $this->updatedDate = $jsonStory->fields->updated;
    
    $this->developers = $jsonStory->fields->customfield_11000;
    if ($this->developers==""){
      $this->developers=1;
    }
    
    if (property_exists ($jsonStory, 'changelog')){
      $histories = count($jsonStory->changelog->histories)-1;
     $this->statusChangeDate =  $jsonStory->changelog->histories[$histories]->created;
    }
    
  }
  
 public function printStoryCard(){
    
    
    $cardType = "danger";
    $icon = "";
    if ($this->type=="Improvement"){
      $cardType = "success";
      $icon = "arrow-up";
    }
    if ($this->type=="Story"){
      $cardType = "success";
      $icon = "bookmark";
    }   
    if ($this->type=="Bug"){
      $cardType = "danger";
      $icon = "record";
    }  
    if ($this->type=="Operations"){
      $cardType = "info";
      $icon = "cog";
    }  
    if ($this->type=="Maintenance"){
      $cardType = "primary";
      $icon = "wrench";
    }     
   

    
    $time = $this->printTime($this->statusChangeDate);
    
    echo <<< EOF
<div class='storyCard col-lg-3 col-md-4 col-sm-6 col-xs-12'>
  <div class="panel panel-default panel-$cardType $this->type">
    <div class="panel-heading text-center"><span class="glyphicon glyphicon-$icon" aria-hidden="true"></span> $this->key</div>
    <div class="panel-body">
     $this->summary
    </div>
    <div class="panel-footer">
    In Review since: $time
        
    </div>
  </div>
</div>
EOF;

    
  }
  
  private function printTime($timestamp){
    $now = new DateTime("now");
    $date1 = new DateTime($timestamp);
    $intervalSec = time() - strtotime($timestamp);
    $interval = $now->diff($date1);

    if ($intervalSec < (60)){
      return $interval->format('%s seconds ago');
    }
    if ($intervalSec < (60*60)){
      return $interval->format('%i minutes ago');
    }
    if ($intervalSec < (60*60*24)){
      return $interval->format('%h hours ago');
    }
    return $interval->format('%d days ago');
       
  }
  

}