<!DOCTYPE html>
<html>
<head>
	<title>New Relic</title>
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

  <link rel="stylesheet" type="text/css" href="css/style.css">
  
  </head>
<body>		
    
    <nav class="navbar navbar-default navbar-static-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <p class="navbar-text">New Relics Reports</p>
    </div>
  </div>
</nav>
  
<div class="container-fluid">
<?php
  
  $newRelicCharts = ['9Ho7eVXOciF','9XdT7xK51YX','f6ZUQrgnotq','2XocImSZ9b8'];
  
  foreach ($newRelicCharts as $chart) {
    echo <<< EOF
    <div class="row">
      <div class='col-xs-12'> 
        <div class="panel panel-default">
          <iframe width="100%" height="190px" src="https://rpm.newrelic.com/public/charts/$chart" frameborder="0"></iframe>
        </div>
      </div>
    </div>    
EOF;

  }
?>

 </div>
</body>
</html>
