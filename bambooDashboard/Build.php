<?php
class Build {

  public $name = '';
  public $enabled = '';
  public $summary = '';
  public $status = '';
  public $project = '';
  public $buildReason = '';
  public $reasonSummary = '';
  public $buildState = '';
  public $buildCompletedTime = '';
  public $buildNumber = '';
  public $planKey = "";

  public function buildFromBranch($branch){
    $this->buildGeneric($branch);
    if (property_exists ($branch,"latestResult")){
      $this->project = $branch->latestResult->projectName;
      $this->buildState = $branch->latestResult->buildState;
      $this->buildNumber = $branch->latestResult->buildNumber;
      $this->buildCompletedTime = $branch->latestResult->buildCompletedTime;
      $this->buildReason = $branch->latestResult->buildReason;
      $this->reasonSummary = $branch->latestResult->reasonSummary;    
    }

    if (property_exists ($branch,"latestCurrentlyActive")){
      if ($branch->latestCurrentlyActive->lifeCycleState=="InProgress"){
        $this->status = "InProgress";
      }
      else{
        $this->status = "Queued";
      }            
    }
    else {
      $this->status = "Inactive";
    }
    
  }
  
  public function buildFromPlan($plan){
    $this->buildGeneric($plan);
    $this->project = $plan->projectName;
    $this->planKey = $plan->key;
    
    if ($this->enabled){
      $result = getCURLDataPlan($this->planKey);
      $strDatas = json_decode($result);
      $planJson = $strDatas->results->result[0];
//       echo "<pre>";
//       print_r($planJson);
//       echo "</pre>";
      $this->buildState = $planJson->buildState;
      $this->buildNumber = $planJson->buildNumber;
      $this->buildCompletedTime = $planJson->buildCompletedTime;
      $this->buildReason = $planJson->buildReason;
      $this->reasonSummary = $planJson->reasonSummary;
    }
    

    if ($plan->isBuilding){
      $this->status = "InProgress";
    }
    else{
      if ($plan->isActive){
        $this->status = "Queued";
      }
      else {
        $this->status = "Inactive";
      }
    }
    
  }
  
  private function buildGeneric($plan){
    if ($plan->enabled){
      $this->enabled = true;
    }
    else{
      $this->enabled = false;
    }
    $this->shortName = $plan->shortName;
    
  }
  
  public function printData(){
    
//     echo "<div class='buildCard'>";
//     echo "<div class='build ".$this->buildState."'>";
//       echo "<div class='name'>".$this->shortName."</div>";

//       echo "<div class='status ".$this->status."'>";
//       if ($this->status!="Inactive"){
//         echo $this->status;
//       }
//       echo "</div>";
//       echo "<div class='project'>".$this->project."</div>";
//       echo "<div class='time'>".$this->printTime($this->buildCompletedTime)."</div>";
//     echo "</div>\n";
//     echo "<div class='reason'>".$this->buildReason."</div>";
//     echo "</div>\n";
    
    $buildState = "warning";
    if ($this->buildState=="Successful"){
      $buildState = "success";
    }
    if ($this->buildState=="Failed"){
      $buildState = "danger";
    }  
    $status = "";
    $statusStyle = "warning";
    $statusPerc = "0";
    $statusAnim = "";
    
      if ($this->status=="Inactive"){
      }
      if ($this->status=="InProgress"){
        $status = "In Progress";
        $statusPerc = "100";
        $statusStyle = "success";
        $statusAnim = "progress-bar-striped active";

      }
      if ($this->status=="Queued"){
        $status = "Queued";
        $statusPerc = "100";
        $statusStyle = "danger";
      
      }
    $time = $this->printTime($this->buildCompletedTime);
    
    echo <<< EOF
<div class='buildCard col-lg-3 col-md-4 col-sm-6 col-xs-12'>
  <div class="panel panel-default panel-$buildState">
    <div class="panel-heading text-center">$this->shortName</div>
    <div class="panel-body">
      <div class="progress">
        <div class="progress-bar progress-bar-$statusStyle $statusAnim" role="progressbar" aria-valuenow="$statusPerc" aria-valuemin="0" aria-valuemax="100" style="width: $statusPerc%;">
          $status
        </div>
      </div>      
    </div>
    <div class="panel-footer">
      <div class="row">
        <div class="col-xs-9"><div class="build-time">$time</div></div>
        <div class="col-xs-3"> <div class="last-build text-right">#$this->buildNumber</div></div>
      </div>    
    </div>
  </div>
  <div class='reason'>$this->buildReason</div>
</div>
EOF;

    
  }
  
  private function printTime($timestamp){
    $now = new DateTime("now");
    $date1 = new DateTime($timestamp);
    $intervalSec = time() - strtotime($this->buildCompletedTime);
    $interval = $now->diff($date1);

    if ($intervalSec < (60)){
      return $interval->format('%s seconds ago');
    }
    if ($intervalSec < (60*60)){
      return $interval->format('%i minutes ago');
    }
    if ($intervalSec < (60*60*24)){
      return $interval->format('%h hours ago');
    }
    return $interval->format('%d days ago');
       
  }
  
}