<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="refresh" content="10">
	<title>Builds</title>

  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

  <link href='https://fonts.googleapis.com/css?family=Source+Code+Pro' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Nunito' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  
  </head>
<body>	
  
<nav class="navbar navbar-default navbar-static-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <p class="navbar-text">Bamboo Builds</p>
    </div>
  </div>
</nav>
  
  
  <div class="container-fluid">
    <div class="row">

<?php
require_once 'util.php';
require_once 'Build.php';


$result = getCURLData();
$strDatas = json_decode($result);
$plans = $strDatas->plans->plan;
// echo "<pre>";
// print_r($plans);
// echo "</pre>";

  
foreach ($plans as $plan){
  $planObj = new Build();
  $planObj->buildFromPlan($plan);
  if ($planObj->enabled){
    $planObj->printData();
  }
  
  if ($plan->branches->size>0){
    foreach ($plan->branches->branch as $branch){
      $branchObj = new Build();
      $branchObj->buildFromBranch($branch);
      if ($branchObj->enabled){
        $branchObj->printData();
      }
    }
  }
  
}
  
?>
      </div>
    </div>
</body>
</html>
  
  
