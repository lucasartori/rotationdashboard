<?php


function getCURLData(){
  $url = "https://leisure.atlassian.net/builds/rest/api/latest/plan.json?expand=plans.plan.branches.branch.latestResult";
  return getCURL($url);

}

function getCURLDataPlan($plan){
  
  $url = "https://leisure.atlassian.net/builds/rest/api/latest/result/".$plan.".json?expand=results.result.latestResult";
  return getCURL($url);
}

function getCURL($url){

	$headers = array(
    'Accept: application/json',
    'Content-Type: application/json',
    'Authorization: Basic YmVsdmlsbGEtYWxsOmhlM2c0UlZreWthSw=='
  );

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_VERBOSE, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);  
  curl_setopt($ch, CURLOPT_URL, $url);

  /* execute the request */
  $result = curl_exec($ch);
  curl_close($ch);
  return $result;
}


function convertReports($jsonIssues){
  $issues =[];
  foreach ($jsonIssues as $story) {
    array_push ($issues, new Report($story));
  }
  return $issues;
}
